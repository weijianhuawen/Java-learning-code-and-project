create database if not exists java_gobang;
use java_gobang;

drop table if exists user;
create table user (
    -- id
    uid int primary key auto_increment,
    -- 用户名
    username varchar(50) unique ,
    -- 邮箱
    email varchar(128) unique,
    -- 手机号
    numbers varchar(20) unique,
    -- 密码
    password varchar(50),
    -- 战绩
    score int,
    -- 对战场数
    totalCount int,
    -- 获胜场数
    winCount int,
    -- 胜率
    winRate double(8, 2)
);

insert into user values(null, 'zhangsan', '', '', '122333', 111, 22, 2, 0.00);
insert into user values(null, 'lisi',  '111', '111', '1223', 11, 2, 0, 0.00);
insert into user values(null, 'wangwu',  '222', '222', '122333', 1122, 222,92, 0.00);
insert into user values(null, 'wwww',  '333', '333', '122333', 1211, 2222, 542, 0.00);
insert into user values(null, 'chatGPT',  '333', '333', '122333', 1500, 0, 0, 0.00);