package com.example.gobangdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class GobangdemoApplication {
	//使用该对象来获取spring启动时返回的参数，可以通过该对象手动注入
	public static ConfigurableApplicationContext context;

	public static void main(String[] args) {
		context = SpringApplication.run(GobangdemoApplication.class, args);
	}

}
