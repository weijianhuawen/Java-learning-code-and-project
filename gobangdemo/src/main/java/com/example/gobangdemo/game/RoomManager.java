package com.example.gobangdemo.game;

import com.example.gobangdemo.mode.User;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;

@Component
public class RoomManager {
    //房间编号与房间
    private final ConcurrentHashMap<String, Room> rooms = new ConcurrentHashMap<>();
    //用户id映射房间id
    private final ConcurrentHashMap<Integer, String> userIdToRoomId = new ConcurrentHashMap<>();
    // 添加房间
    public void add(Room room, User[] users) {
        rooms.put(room.getRid(), room);
        for (int i = 0; i < users.length; i++) {
            userIdToRoomId.put(users[i].getUid(), room.getRid());
        }
    }
    // 移除房间
    public void remove(Room room) {
        if (room == null) return;
        rooms.remove(room.getRid());
        User[] users = room.getUsers();
        if (users == null) return;
        for (int i = 0; i < users.length; i++) {
            userIdToRoomId.remove(users[i].getUid());
        }
    }
    // 查询房间
    public Room getRoom(String rid) {
        return rooms.getOrDefault(rid, null);
    }
    public Room getRoom(Integer uid) {
        //获取对应的rid
        String rid = userIdToRoomId.getOrDefault(uid, "");
        return getRoom(rid);
    }
}
