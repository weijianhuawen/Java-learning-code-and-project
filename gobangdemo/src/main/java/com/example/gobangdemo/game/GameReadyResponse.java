package com.example.gobangdemo.game;

import lombok.Data;

@Data
public class GameReadyResponse {
    private String message;
    private boolean ok;
    private String reason;
    private String rid;
    private Integer thisUserId;
    private Integer thatUserId;
    private boolean isWhite;
}
