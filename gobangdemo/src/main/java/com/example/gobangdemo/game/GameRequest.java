package com.example.gobangdemo.game;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

import java.util.IllegalFormatException;

//落子请求
@Data
public class GameRequest {
    private String message;
    private Integer uid;
    private Integer row;
    private Integer col;
    private Integer maxRow;
    private Integer maxCol;
}
