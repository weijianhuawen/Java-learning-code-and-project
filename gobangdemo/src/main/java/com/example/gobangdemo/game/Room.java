package com.example.gobangdemo.game;

import com.example.gobangdemo.GobangdemoApplication;
import com.example.gobangdemo.mapper.UserMapper;
import com.example.gobangdemo.mode.User;
import com.example.gobangdemo.tools.SpringUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import javax.annotation.Resource;
import javax.jws.soap.SOAPBinding;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.UUID;

//游戏房间对象
@Data
@Slf4j
public class Room {
    private final ObjectMapper objectMapper = new ObjectMapper();
    //因为Room类不是spring的一个组件，所以不能使用注解进行自动注入，需要手动注入
    //又因为Room不能是单例，所以也不适合将Room设置为一个组件，所以需要将所依赖的管理手动注入
    //使用tools中的springUtil类进行手动注入
    //@Resource
    private OnlineUserManager onlineUserManager;
    //@Resource
    private RoomManager roomManager;

    //mapper
    private UserMapper userMapper;
    //房间id
    private String rid;
    //房间玩家数 默认为2
    private int n = 2;
    //房间玩家
    private User[] users;

    //先手方的游戏id
    private Integer PriorityUid;

    //棋盘
    private int[][] board;
    //记录下棋的总数，方便判断棋盘沾满出现平局的情况
    private int total = 0;

    private void setUsers(User[] users) {
        this.users = users;
    }

    //设置棋盘大小
    //0表示当前位置未落子 1玩家1落子位置 2玩家2；落子位置
    public void setBoard(int row, int col) {
        if (board == null) board = new int[row][col];
    }
    //强制修改棋盘大小
    public void setForceBoard(int row, int col) {
        //复制原始数据
        int[][] temp = new int[row][col];
        int n = board.length;
        int m = board.length;

        for (int i = 0; i < row && i < n; i++) {
            temp[i] = Arrays.copyOf(board[i], col);
        }
        board = temp;
        log.info("棋盘大小已经被强制修改");
    }
    /**
     * 处理落子操作 ： 记录落子的位置 进行胜负判定 返回响应
     * @param payload
     */
    public void putChess(String payload) throws IOException {
        GameRequest req = objectMapper.readValue(payload, GameRequest.class);
        GameResponse resp = new GameResponse();
        //初始化棋盘大小
        log.info("putchess" + req);
        setBoard(req.getMaxRow(), req.getMaxCol());
        //当前这个字是玩家1落的还是玩家2落的子
        int chess = Objects.equals(req.getUid(), users[0].getUid()) ? 1 : 2;
        //获取下棋落子的坐标
        int row = req.getRow();
        int col = req.getCol();

        if (board[row][col] != 0) {
            //表示当前位置已经有子了
            log.info("位置(" + row + ", " + col + ")已经存在棋子，不可继续下子");
            return;
        }
        //落子
        board[row][col] = chess;
        //判定胜负
        User winner = checkWinner(row, col, chess);
        //输出棋盘
        printChessBoard();
        //给房间中的所有玩家都返回响应
        resp.setMessage("putChess");
        resp.setUid(req.getUid());
        resp.setRow(row);
        resp.setCol(col);
        resp.setWinner(winner == null ? 0 : winner.getUid());
        resp.setWinnerName(winner == null ? "" : winner.getUsername());
        //需要发送给所有的用户，需要websocket session
        //0 1位置对应的玩家为实时对战玩家，其他玩家为观战玩家
        System.out.println(users[0]);
        System.out.println(users[1]);
        System.out.println(onlineUserManager);
        WebSocketSession session1 = onlineUserManager.getGameRoom(users[0].getUid());
        WebSocketSession session2 = onlineUserManager.getGameRoom(users[1].getUid());

        //判断是否存在玩家掉线
        if (session1 == null) {
            //玩家1掉线表示玩家2直接获胜
            resp.setWinner(users[1].getUid());
            log.info("玩家1掉线，玩家2获胜");
        }

        if (session2 == null) {
            //玩家2掉线，表示玩家1获胜
            resp.setWinner(users[0].getUid());
            log.info("玩家2掉线，玩家1获胜");
        }

        //向所有的玩家以及观众放回响应结果
        for (int i = 0; i < n; i++) {
            WebSocketSession session = onlineUserManager.getGameRoom(users[i].getUid());
            if (session != null) {
                session.sendMessage(new TextMessage(objectMapper.writeValueAsString(resp)));
            }
        }

        //如果胜负已经分出，则游戏结束并销毁房间
        if (resp.getWinner() != 0) {
            int winId = resp.getWinner();
            if (winner != null && winId != -1) {
                User loser = winId == users[0].getUid() ? users[1] : users[0];
                int loseId = loser.getUid();
                //修改数据库 两个用户的分数
                // 修改获胜者的信息 total + 1 win + 1 天梯分数加上32 + 与对手天梯分之差的20% 如果对手天梯分高 差值加分幅度上升至32%
                // 修改失败者的信息 total + 1 win + 0 天梯分分数加上10 - (232 - 与对手天梯分之差) 的20% 如果你的天梯分高 再减去对手低于你的分数差值的22%
                int winScore = 32 + (int) (Math.abs(loser.getScore() - winner.getScore()) * 0.2);
                if (loser.getScore() > winner.getScore()) winScore += (int) ((winScore - 30) * 0.6);
                //修改胜利者分数
                //计算胜率
                double winRate = (1.0 * (winner.getWinCount() + 1) / (winner.getTotalCount() + 1)) * 100;
                log.info(winId + " " + winScore + " " + winRate + " ");
                userMapper.updateUserWin(winId, winScore);
                int loserScore = 10 - (int) ((232 - (Math.abs(loser.getScore() - winner.getScore()))) * 0.2);
                if (loser.getScore() > winner.getScore()) loserScore -= (int)(Math.abs(loser.getScore() - winner.getScore())) * 0.22;
                //修改失败者分数
                //计算胜率
                winRate = (1.0 * loser.getWinCount() / (loser.getTotalCount() + 1)) * 100;
                log.info(loseId + " " + loserScore + " " + winRate + " ");
                userMapper.updateUserLose(loseId, loserScore);
            }
            log.info("游戏胜负已分，销毁房间，退出游戏，返回到游戏大厅");
            roomManager.remove(this);
        }
    }

    private User checkWinner(int row, int col, int chess) {
        if (board == null) {
            //棋盘未初始化
            log.info("验证时，棋盘未初始化");
            return null;
        }
        int maxRow = board.length;
        int maxCol = board[0].length;
        if (row < 0 || col < 0 || col >= maxCol || row >= maxRow) {
            //下棋的坐标越界
            log.info("下棋下标越界!");
            return null;
        }
        //检查是否出现五星连珠
        for (int j = Math.max(0, col - 4); j <= col && j + 4 < maxCol; j++) {
            //检查横排
            if (board[row][j] == chess && board[row][j + 1] == chess
                    && board[row][j + 2] == chess && board[row][j + 3] == chess
                    && board[row][j + 4] == chess) {
                //表示chess对应的下棋方，横线五星连珠
                return users[chess - 1];
            }
        }
        //检查竖排
        for (int i = Math.max(0, row - 4); i <= row && i + 4 < maxRow; i++) {
            if (board[i][col] == chess && board[i + 1][col] == chess
                    && board[i + 2][col] == chess && board[i + 3][col] == chess
                    && board[i + 4][col] == chess) {
                return users[chess - 1];
            }
        }
        //检查正对角线
        for (int i = row - 4, j = col - 4; i <= row && j <= col && i + 4 < maxRow && j + 4 < maxCol; i++, j++) {
            if (i < 0 || j < 0) continue;
            if (board[i][j] == chess && board[i + 1][j + 1] == chess
                    && board[i + 2][j + 2] == chess && board[i + 3][j + 3] == chess
                    && board[i + 4][j + 4] == chess) {
                return users[chess - 1];
            }
        }
        //检查反对角线 斜上方方向滑动
        for (int i = row + 4, j = col - 4; i >= row && j <= col; j++, i--) {
            if (i >= maxRow || j < 0 || i - 4 < 0 || j + 4 > maxCol) continue;

            if (board[i][j] == chess && board[i - 1][j + 1] == chess
                    && board[i - 2][j + 2] == chess && board[i - 3][j + 3] == chess
                    && board[i - 4][j + 4] == chess) {
                return users[chess - 1];
            }
        }
        total += 1;
        //考虑平局 也就是棋盘下满了
        if (total == maxRow * maxCol) {
            //棋盘全部满了 返回请求给前端表示平局
            //使得返回user对象的id为-1，区分平局
            User user = new User();
            user.setUsername("平局");
            user.setUid(-1);
            return user;
        }
        return null;
    }

    public Room() {
        //使用uuid生成唯一id
        n = 2;
        users = new User[n];
        rid = UUID.randomUUID().toString();

        //手动注入对象
//        onlineUserManager = SpringUtil.getBean(OnlineUserManager.class);
//        roomManager = SpringUtil.getBean(RoomManager.class);
        onlineUserManager = GobangdemoApplication.context.getBean(OnlineUserManager.class);
        roomManager = GobangdemoApplication.context.getBean(RoomManager.class);
        userMapper = GobangdemoApplication.context.getBean(UserMapper.class);
    }

    //添加某一位玩家
    public boolean addPlayer(int index, User user) {
        if (index >= n) {
            log.info("房间只有0-" + (n - 1) + "的编号，传入的编号非法越界");
            return false;
        }
        if (users[index] != null) {
            //覆盖前一个玩家
            log.info("玩家：" + users[index].getUsername() + "被覆盖");
        }
        users[index] = user;
        return true;
    }

    //获取某一个位置的玩家
    public User getPlayer(int index) {
        return users[index];
    }

    //打印棋盘，便于调试
    public void printChessBoard() {
        if (board == null) {
            //棋盘还未初始化
            System.out.println("棋盘还未初始化!");
            return;
        }
        System.out.println(this.rid + "棋盘信息如下：");

        int n = board.length;
        int m = board[0].length;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.print(board[i][j] + "  ");
            }
            System.out.println();
        }
    }
}
