package com.example.gobangdemo.game;

import lombok.Data;

//落子响应
@Data
public class GameResponse {
    private String message;
    private String winnerName;
    private Integer uid;
    private Integer row;
    private Integer col;
    private Integer winner;
    private boolean ok;
}
