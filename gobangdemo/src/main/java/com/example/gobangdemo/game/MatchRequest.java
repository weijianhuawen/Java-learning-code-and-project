package com.example.gobangdemo.game;

import lombok.Data;

//websocket请求
@Data
public class MatchRequest {
    private String message = "";
}
