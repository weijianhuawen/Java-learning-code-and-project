package com.example.gobangdemo.game;

import lombok.Data;

//websocket响应
@Data
public class MatchResponse {
    private boolean ok;
    private String reason;
    private String message;
}
