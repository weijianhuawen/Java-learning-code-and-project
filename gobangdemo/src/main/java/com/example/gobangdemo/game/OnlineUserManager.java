package com.example.gobangdemo.game;

import com.example.gobangdemo.config.WebSocketConfig;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class OnlineUserManager {
    //当前哈希表就来表示当前游戏大厅中的在线状态
    //仅使用哈希表在不考虑多线程的情况下是靠谱的，但是如果处于多线程情况下，不见得会靠谱，毕竟HashMap是线程不安全的
//    private HashMap<Integer, WebSocketSession> gameHall = new HashMap<>();
    private final ConcurrentHashMap<Integer, WebSocketSession> gameHall = new ConcurrentHashMap<>();

    //管理游戏房间当中游戏玩家的在线状态
    private final ConcurrentHashMap<Integer, WebSocketSession> gameRoom = new ConcurrentHashMap<>();

    public void enterGameHall(Integer userId, WebSocketSession session) {
        gameHall.put(userId, session);
    }

    public void exitGameHall(Integer userId) {
        gameHall.remove(userId);
    }

    public WebSocketSession getGameHall(Integer userId) {
        return gameHall.getOrDefault(userId, null);
    }

    public void enterGameRoom(Integer userId, WebSocketSession session) {
        gameRoom.put(userId, session);
    }

    public void exitGameRoom(Integer userId) {
        gameRoom.remove(userId);
    }

    public WebSocketSession getGameRoom(Integer userId) {
        return gameRoom.getOrDefault(userId, null);
    }
}
