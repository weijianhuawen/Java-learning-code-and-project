package com.example.gobangdemo.game;

import com.example.gobangdemo.mode.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

//匹配器
@Component
@Slf4j
public class Matcher {
    private ObjectMapper objectMapper = new ObjectMapper();
    //创建多个匹配队列
    private static final int N = 12;
    private final Queue<User>[] queue = new LinkedList[N];

    @Autowired
    private OnlineUserManager onlineUserManager;
    @Autowired
    private RoomManager roomManager;

    //初始化
    private void init() {
        for (int i = 0; i < N; i++) queue[i] = new LinkedList<>();
    }

    //创建线程，分配任务
    private Thread creatThread(Queue<User> workQueue) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                //扫描目标队列
                //直接循环等待对于CPU开销极大，需要进行优化
                //解决方案1:使用sleep()减少刷新的频率，大大降低CPU的开销，但用户会存在一定的等待时间，会降低用户的体验，就像王者排位时，硬是要等待一段时间，而不能秒排
                //解决方案2：使用wait/ontify,当有玩家进入队列时，我们才通知线程执行，这是最优解，我们采取该方案
                while (true) {
                    handerMatch(workQueue);
                }
            }
        };
        return thread;
    }

    private void handerMatch(Queue<User> workQueue) {
        //其他服务使用相对应队列的add，remove时，我们不进行匹配处理，以免造成线程安全问题
        synchronized (workQueue) {
            //检测队列中的玩家数是否大于2，如果过大于2，取出两位玩家进行匹配
            //如果队列中的玩家数量少于2，使用wait进行等待，直到有新玩家进入匹配
            while (workQueue.size() < 2) {
                try {
                    workQueue.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            //取出两位玩家
            User player1 = workQueue.poll();
            User player2 = workQueue.poll();
            log.info("玩家1：" + player1.getUsername() + "与玩家2：" + player2.getUsername() + "进行匹配!");

            //获取到两位玩家的websocket会话
            WebSocketSession session1 = onlineUserManager.getGameHall(player1.getUid());
            WebSocketSession session2 = onlineUserManager.getGameHall(player2.getUid());

            //判断会话是否为null
            //理论上来说，存在与队列中的玩家一定是在线的
            //因为玩家断开连接的时候，会自动将玩家从匹配队列中移除
            //为了保险起见，我们进行双重判定
            //两位玩家均不在线
            if (session1 == null && session2 == null) {
                log.info("匹配时两位玩家均不在线!");
                return;
            }
            if (session1 == null) {
                //如果玩家1不在线，将玩家2重新加入到匹配队列
                log.info("玩家1匹配时不在线!");
                workQueue.offer(player2);
                return;
            }
            if (session2 == null) {
                log.info("玩家2匹配时不在线!");
                workQueue.offer(player1);
                return;
            }

            //排除两位玩家是同一个用户的情况
            //理论上不存在，因为玩家下线会移除队列，也禁止玩家禁止多开
            //双重判定，防止前面的逻辑出现错误
            if (session1 == session2) {
                //随机放回一个玩家
                log.info("匹配玩家相同");
                workQueue.offer(player1);
            }

            //TODO 把两位玩家放入至同一个房间当中
            User[] users = {player1, player2};
            //因为此时无法确认两位玩家是否已经成功到游戏房间页面，暂时不能将玩家放入房间中
            //需要等到验证玩家进入游戏房间后我们再来将两位玩家加入房间
            //room.setUsers(users);
//            room.addPlayer(0, player1);
//            room.addPlayer(1, player2);
            Room room = new Room();
            room.setN(users.length);
            roomManager.add(room, users);

            //给玩家反馈信息
            //通过websocket返回一个结果
            MatchResponse response1 = new MatchResponse();
            MatchResponse response2 = new MatchResponse();
            try {
                response1.setOk(true);
                response1.setReason("匹配成功!");
                response1.setMessage("matchSuccess");
                session1.sendMessage(new TextMessage(objectMapper.writeValueAsString(response1)));
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                response2.setOk(true);
                response2.setReason("匹配成功!");
                response2.setMessage("matchSuccess");
                session2.sendMessage(new TextMessage(objectMapper.writeValueAsString(response2)));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //构造方法
    public Matcher() {
        init();
        //创建线程，将这些队列以多线程的方式维护起来
        for (int i = 0; i < N; i++) {
            log.info("第" + (i + 1) + "个队列的线程启动!");
            creatThread(queue[i]).start();
        }

    }
    //操作匹配队列
    //获取具体玩家的匹配通道队列
    private int getUserQueue(User user) {
        //获取等级分
        int idx = -1;
        int score = user.getScore();
        if (score < 1000) idx = 0;
        else if (score < 1200) idx = 1;
        else if (score < 1400) idx = 2;
        else if (score < 1600) idx = 3;
        else if (score < 1800) idx = 4;
        else if (score < 2000) idx = 5;
        else if (score < 2200) idx = 6;
        else if (score < 2400) idx = 7;
        else if (score < 2600) idx = 8;
        else if (score < 2800) idx = 9;
        else if (score < 3000) idx = 10;
        else idx = 11;

        return idx;
    }

    //进入匹配队列
    public void add(User user) {
        int idx = getUserQueue(user);
        Queue<User> q = queue[idx];
        //在处理匹配和进入匹配以及退出匹配时，可能存在同时操作add或者remove，存在线程安全问题，需要加锁
        synchronized (q) {
            q.offer(user);
            //遇到新元素入队，通知刷新
            q.notify();
        }
        log.info("用户：" + user.getUsername() + ", 分数为：" + user.getScore() + "进入第" + (idx+1) + "条匹配赛道!");

    }

    //退出匹配队列
    public void remove(User user) {
        //当玩家点击停止匹配，需要将玩家删除
        int idx = getUserQueue(user);
        Queue<User> q = queue[idx];
        //在处理匹配和进入匹配以及退出匹配时，可能存在同时操作add或者remove，存在线程安全问题，需要加锁
        synchronized (q) {
            q.remove(user);
        }
        log.info("用户：" + user.getUsername() + ", 分数为：" + user.getScore() + "退出第" + (idx+1) + "条匹配赛道!");

    }



}
