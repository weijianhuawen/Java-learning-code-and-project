package com.example.gobangdemo.mode;

import lombok.Data;
import org.omg.CORBA.WStringSeqHelper;

@Data
public class User {
    private Integer uid;
    private String username;
    private String email;
    private String numbers;
    private String password;
    private int score;
    private int totalCount;
    private int winCount;
    private Double winRate;

    public User() {}

    public User(String username, String email, String numbers, String password) {
        this.username = username;
        this.email = email;
        this.numbers = numbers;
        this.password = password;
    }
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
