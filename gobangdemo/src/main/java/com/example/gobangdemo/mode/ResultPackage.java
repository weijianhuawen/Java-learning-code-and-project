package com.example.gobangdemo.mode;

import lombok.Data;

@Data
public class ResultPackage<T> {
    private T data;
    //状态
    private Integer sid;
    //信息说明
    private String message;
    //是否成功
    private boolean isOk;

    //构造方法
    public ResultPackage() {
        data = null;
        sid = 400;
        message = "发生未知错误";
        isOk = false;
    }

    public ResultPackage(T data, Integer sid, String message, boolean isOk) {
        this.data = data;
        this.sid = sid;
        this.message = message;
        this.isOk = isOk;
    }
}
