package com.example.gobangdemo.config;

import com.example.gobangdemo.api.GameApi;
import com.example.gobangdemo.api.MatchApi;
import com.example.gobangdemo.api.TestApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {
    @Autowired
    TestApi testApi;
    @Autowired
    private MatchApi matchApi;
    @Autowired
    private GameApi gameApi;

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(testApi, "/test");
        //将matchAPI注册到websocket中
        registry.addHandler(matchApi, "/findMatch")
                //在注册websocket时需将当前用户的session获取过来，存入到websocket中的session中
                .addInterceptors(new HttpSessionHandshakeInterceptor());

        registry.addHandler(gameApi,"/game")
                .addInterceptors(new HttpSessionHandshakeInterceptor());
    }
}
