package com.example.gobangdemo.api;

import com.example.gobangdemo.game.MatchRequest;
import com.example.gobangdemo.game.MatchResponse;
import com.example.gobangdemo.game.Matcher;
import com.example.gobangdemo.game.OnlineUserManager;
import com.example.gobangdemo.mode.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import javax.annotation.Resource;
import javax.jws.soap.SOAPBinding;
import java.sql.Struct;
//通过该类来处理websocket请求

@Component
@Slf4j
public class MatchApi extends TextWebSocketHandler {
    private ObjectMapper objectMapper = new ObjectMapper();
    @Resource
    private OnlineUserManager onlineUserManager;
    @Autowired
    private Matcher matcher;
    //将上线和下线操作逻辑进行封装
    private void online(WebSocketSession session) throws Exception{
        //玩家上线加入到OnlineUserManager中
        //获取用户id
        //通过websocket中session获取用户信息
        //此前已经将HTTP用户session传到websocket中了
        //处理多开问题
        try {
            User user = (User) session.getAttributes().get("user");
            //判断当前用户是否已经登录过，如果已经登录就禁止登录
            //TODO 注意当用户在游戏大厅在线和游戏房间在线都视为已经登录
            if (onlineUserManager.getGameHall(user.getUid()) != null || onlineUserManager.getGameRoom(user.getUid()) != null) {
                //查询有结果表示已经登录
                log.warn(user.getUsername() + "在多处登录!");
                MatchResponse matchResponse = new MatchResponse();
                matchResponse.setOk(true);
                matchResponse.setReason("你在其他浏览器或者设备已经登录，不能重复登录!");
                matchResponse.setMessage("repeatConnection");
                session.sendMessage(new TextMessage(objectMapper.writeValueAsString(matchResponse)));
                //返回多开情况，交由前端处理
                session.sendMessage(new TextMessage(objectMapper.writeValueAsString(matchResponse)));
                //session.close();
                return;
            }
            //设置在线状态
            log.info("添加" + user.getUsername() + "到websocket在线列表当中!");
            onlineUserManager.enterGameHall(user.getUid(), session);
        } catch (NullPointerException e) {
            log.warn("websocket中获取到的session或者对话内容为空!");
            log.info("用户信息为空!用户未登录!");
            MatchResponse matchResponse = new MatchResponse();
            matchResponse.setOk(false);
            matchResponse.setReason("用户未登录！无法进行在线添加！");
            matchResponse.setMessage(null);
            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(matchResponse)));
        }
    }

    /**
     *
     * @param session websocket session对象
     * @param isSend 是否发送消息到客户端，如果该方法用于下面的close和error，则不能发送，因为连接已经关闭了
     * @throws Exception
     */
    private void offline(WebSocketSession session, boolean isSend) throws Exception{
        User user = null;
        try {
            user = (User) session.getAttributes().get("user");
            //避免处理多开时因关闭websocket连接而去删除原来的session
            WebSocketSession tempSession = onlineUserManager.getGameHall(user.getUid());
            if (tempSession == session) {
                log.info("移除" + user.getUsername() + "到websocket在线列表当中!");
                onlineUserManager.exitGameHall(user.getUid());
            }
            //TODO 如果玩家正在匹配中，但因为某些原因被强制关闭，需要将玩家退出队列
            matcher.remove(user);
        } catch (NullPointerException e) {
            log.warn("websocket中获取到的session或者对话内容为空!");
            log.info("用户信息为空!用户未登录!");
            MatchResponse matchResponse = new MatchResponse();
            matchResponse.setOk(false);
            matchResponse.setReason("当前用户尚未登录!");
            matchResponse.setMessage(null);
            if (isSend) session.sendMessage(new TextMessage(objectMapper.writeValueAsString(matchResponse)));
        }
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        online(session);
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        //处理开始匹配请求和处理停止匹配请求
        log.info("处理开始匹配请求和处理停止匹配请求");
        //获取用户信息
        User user = (User) session.getAttributes().get("user");
        //获取客户端发送给后端的请求
        String playload = message.getPayload();
        //约定数据载荷为JSON字符串
        MatchRequest matchRequest = (MatchRequest) objectMapper.readValue(playload, MatchRequest.class);
        log.info(matchRequest.getMessage());
        //准备响应对象
        MatchResponse matchResponse = new MatchResponse();
        if ("startMatch".equals(matchRequest.getMessage())) {
            //进入匹配队列
            // TODO 先创建一个类表示匹配队列，把当前用户加进去
            matcher.add(user);
            matchResponse.setOk(true);
            matchResponse.setReason("加入匹配队列成功!");
            matchResponse.setMessage("startMatch");
        } else if ("stopMatch".equals(matchRequest.getMessage())) {
            //退出匹配队列
            // TODO 将用户从匹配队列中移除
            matcher.remove(user);
            matchResponse.setOk(true);
            matchResponse.setReason("退出匹配成功!");
            matchResponse.setMessage("stopMatch");
        } else {
            //其他未知错误
            matchResponse.setOk(false);
            matchResponse.setReason("传入请求非法!发生未知错误!");
            matchResponse.setMessage(null);
        }
        //将结果返回客户端!!!
        session.sendMessage(new TextMessage(objectMapper.writeValueAsString(matchResponse)));
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        offline(session, false);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        offline(session, false);
    }
}
