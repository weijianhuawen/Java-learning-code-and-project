package com.example.gobangdemo.api;

import com.example.gobangdemo.mapper.UserMapper;
import com.example.gobangdemo.mode.ResultPackage;
import com.example.gobangdemo.mode.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
@Slf4j
public class UserApi {
    @Autowired
    private UserMapper userMapper;

    @PostMapping("/login")
    @ResponseBody
    public Object login(String username, String password, HttpServletRequest request) {
        if (username == null || username.equals("") || password == null || password.equals("")) return new ResultPackage<User>(null, -1, "用户名或密码不能为空!", false);

        User user = userMapper.userSelectByName(username);
        log.info(username);
        if (user == null || !user.getPassword().equals(password)) {
            String message = "账号不存在或者密码错误!";
//            System.out.println(message);
            log.info(message);
            return new ResultPackage<User>(null, -1, message, false);
        }
        //cookie,没有会话则创建会话
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("user", user);
        //验证通过
        //不能将密码传到前端
        user.setEmail("");
        user.setNumbers("000000");
        user.setPassword("");
        return new ResultPackage<User>(user, 100, "登录成功!", true);
    }

    @PostMapping("/register")
    @ResponseBody
    public Object register(User user, String repeatPassword) {
        if (user== null ||
                user.getUsername() == null || user.getUsername().equals("") ||
                user.getPassword() == null || user.getPassword().equals("") ||
                user.getEmail() == null || user.getEmail().equals("") ||
                user.getNumbers() == null || user.getNumbers().equals("")) {
            return new ResultPackage<User>(null, -1, "用户名或邮箱或手机号或密码不能为空!", false);
        }
        log.info(user.toString());
        log.info(repeatPassword);
        //验证密码与确认密码是否一致
        if (repeatPassword == null || !repeatPassword.equals(user.getPassword())) {
            return new ResultPackage<User>(null, -1, "两次输入密码不一致!", false);
        }
        //验证用户是否存在
        try {
            userMapper.add(user);
        } catch (org.springframework.dao.DuplicateKeyException e) {
            //抛出该异常表示用户已经存在
            user.setEmail("");
            user.setNumbers("000000");
            user.setPassword("");
            return new ResultPackage<User>(user, -1, "用户已存在!", false);
        }
        //将密码手机号邮箱隐藏
        user.setEmail("");
        user.setNumbers("000000");
        user.setPassword("");
        return new ResultPackage<User>(user, 1, "注册成功!", true);
    }

    @GetMapping("/userinfo")
    @ResponseBody
    public Object getUserInfo(HttpServletRequest request){
        //通过Session获取
        HttpSession session = request.getSession(false);
        ResultPackage<User> resultPackage = new ResultPackage<>(null, -1, "用户未登录!", false);
        User user = null;
        try {
            user = (User) session.getAttribute("user");
        } catch (NullPointerException e) {
            log.info("session为空!");
            resultPackage.setMessage("Session获取失败!");
            return resultPackage;
        }
        if (user == null || user.getUsername().equals("")) {
            //重定向至登录页面
            return  resultPackage;
        }
        log.info(user.getUsername() + "获取成功!");
        //从数据库中查询最新的信息返回给客户端
        User lastUser = userMapper.userSelectByName(user.getUsername());
        resultPackage.setData(lastUser);
        resultPackage.setMessage("获取成功!");
        resultPackage.setOk(true);
        resultPackage.setSid(1);
        return resultPackage;
    }
}
