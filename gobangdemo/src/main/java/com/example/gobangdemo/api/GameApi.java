package com.example.gobangdemo.api;

import com.example.gobangdemo.game.*;
import com.example.gobangdemo.mapper.UserMapper;
import com.example.gobangdemo.mode.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.compat.JrePlatform;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.user.SimpUser;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import javax.jws.soap.SOAPBinding;
import java.io.IOException;
import java.util.Objects;

@Component
@Slf4j
public class GameApi extends TextWebSocketHandler{
    private ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    private RoomManager roomManager;
    @Autowired
    private OnlineUserManager onlineUserManager;
    @Autowired
    private UserMapper userMapper;

    private void online(WebSocketSession session) throws IOException{
        GameReadyResponse readyResponse = new GameReadyResponse();
        //获取到用户的身份信息
        User user = null;
        try {
            user = (User) session.getAttributes().get("user");
            //判定当前用户是否已经在游戏房间
            Room room = roomManager.getRoom(user.getUid());
            if (room == null) {
                //当前我们没有找到对应的房间
                //该玩家没有匹配
                readyResponse.setOk(false);
                readyResponse.setReason("该用户未在游戏房间内!");
                session.sendMessage(new TextMessage(objectMapper.writeValueAsString(readyResponse)));
                return;
            }
        } catch (NullPointerException e) {
            //用户未登录
            log.info("用户未登录!");
            readyResponse.setOk(false);
            readyResponse.setReason("用户未登录!");
            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(readyResponse)));
            return;
        }
        //防止多开问题
        //多开情况: 1)用户未进入匹配队列
        //2）用户未进入游戏房间
        if (onlineUserManager.getGameHall(user.getUid()) != null || onlineUserManager.getGameRoom(user.getUid()) != null) {
            readyResponse.setOk(true);
            readyResponse.setReason("用户在多处登录");
            readyResponse.setMessage("repeatConection");
            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(readyResponse)));
            log.info("用户在多处登录");
            return;
        }

        //设置玩家上线
        onlineUserManager.enterGameRoom(user.getUid(), session);
        //玩家验证是在线的情况下，将玩家加入房间
        Room room = roomManager.getRoom(user.getUid());
        //位置为0的玩家为空，表示玩家1还没有进入房间
        //TODO 可能多个玩家同时进入游戏房间，存在多线程问题，都能会都进入下面这个if语句，这样在游戏房间当中不会存在第二位玩家了
        //加锁 原则是需要竞争的资源是谁就对谁进行加锁
        synchronized (room) {
            if (room.getPlayer(0) == null) {
                //将该玩家加入玩家1的位置
                room.addPlayer(0, user);
                //先进入游戏房间的为先手方
                room.setPriorityUid(user.getUid());

                log.info("房间id：" + room.getRid() + "的玩家1：" + user.getUsername() + "准备就绪");
                return;
            }

            //位置为1的玩家为空，同理
            if (room.getPlayer(1) == null) {
                //将该玩家加入玩家2的位置
                room.addPlayer(1, user);
                log.info("房间id：" + room.getRid() + "的玩家2：" + user.getUsername() + "准备就绪");
                User[] users = room.getUsers();
                if (users == null || users[0] == null || users[1] == null) log.warn("加入玩家后users数组为空");
                //此时两个玩家都加入成功了
                //返回一个websocket的响应数据
                //通知玩家已经准备好了
                //通知玩家1
                noticeGameReady(room, room.getPlayer(0), room.getPlayer(1));
                //通知玩家2
                noticeGameReady(room, room.getPlayer(1), room.getPlayer(0));
                return;
            }

            //如果还有玩家尝试键入房间则返回房间已满
            readyResponse.setOk(false);
            readyResponse.setReason("房间已满");
            readyResponse.setMessage("full");
            log.info("房间已满");
            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(readyResponse)));
        }
    }

    private void noticeGameReady(Room room, User thisPlayer, User thatPlayer) throws IOException{
        GameReadyResponse response = new GameReadyResponse();
        response.setOk(true);
        response.setReason("就绪");
        response.setMessage("gameReady");
        response.setRid(room.getRid());
        response.setThisUserId(thisPlayer.getUid());
        response.setThatUserId(thatPlayer.getUid());
        response.setWhite(Objects.equals(room.getPriorityUid(), thatPlayer.getUid()));
        //返回响应给玩家
        log.info("房间玩家就绪， 通知玩家 ： " + thatPlayer.getUsername());
        WebSocketSession session = onlineUserManager.getGameRoom(thisPlayer.getUid());
        session.sendMessage(new TextMessage(objectMapper.writeValueAsString(response)));
    }

    private User offline(WebSocketSession session) {
        //获取用户信息
        User user = null;
        try {
            user = (User) session.getAttributes().get("user");
            WebSocketSession tempSession = onlineUserManager.getGameRoom(user.getUid());
            //注意避免处理·多开时，将原来的session给删除
            if (tempSession == session) {
                log.info("玩家：" + user.getUsername() + "退出游戏房间");
                onlineUserManager.exitGameRoom(user.getUid());
                //连接已断开，不能返回响应
//                GameResponse resp = new GameResponse();
//                resp.setOk(true);
//                resp.setCol(-1);
//                resp.setRow(-1);
//                resp.setMessage("exit");
//                session.sendMessage(new TextMessage(objectMapper.writeValueAsString(resp)));
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return user;
    }
    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        online(session);
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        //获取session用户信息
        User user = (User) session.getAttributes().get("user");
        if (user == null) {
            //用户未登录或者用户的session被清除，需要重新登录
            return;
        }
        Room room = roomManager.getRoom(user.getUid());
        room.putChess(message.getPayload());
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        User user = offline(session);
        log.info("玩家游戏连接异常");
        //由于当前玩家正在房间对战，前面处理掉线判断胜负，需要客服端点击页面才能进行判断
        //如果某位玩家掉线了，且刚好轮到它下棋，此时服务器端是无法处理掉线判定的
        //掉线的时候，会触发error或者close，因此我们在此处加上一个掉线判断即可
        noticeThatUserWin(user);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        User user = offline(session);
        log.info("玩家退出游戏房间");
        noticeThatUserWin(user);
    }

    private void noticeThatUserWin(User thisUser) throws IOException{
        //获取房间
        Room room = roomManager.getRoom(thisUser.getUid());
        if (room == null) {
            //表示房间已经解散，无需通知对手获胜
            log.info("房间已经解散，无需通知对手获胜");
            return;
        }

        //获取对手的对象
        User[] users = room.getUsers();
        if (users == null) {
            //表示房间未初始化,直接返回
            log.info("房间未初始化，无需通知对手");
            return;
        }
        User thatUser = thisUser.getUid().equals(users[0].getUid()) ? users[1] : users[0];
        //获取对手的Session，用于通知对手
        WebSocketSession session = onlineUserManager.getGameRoom(thatUser.getUid());
        if (session == null) {
            //对手也下线了，无需通知
            log.info("对手已经下线，无需通知！");
            return;
        }
        //通知对手获胜
        GameResponse response = new GameResponse();

        response.setOk(true);
        response.setWinner(thatUser.getUid());
        response.setWinnerName(thatUser.getUsername());
        response.setRow(-1);
        response.setCol(-1);
        response.setUid(thatUser.getUid());
        response.setMessage("putChess");
        //发送
        session.sendMessage(new TextMessage(objectMapper.writeValueAsString(response)));
        //修改天梯积分与场次
        User loser = thisUser;
        User winner = thatUser;
        int loseId = loser.getUid();
        int winId = winner.getUid();
        //修改数据库 两个用户的分数
        // 修改获胜者的信息 total + 1 win + 1 天梯分数加上32 + 与对手天梯分之差的20% 如果对手天梯分高 差值加分幅度上升至32%
        // 修改失败者的信息 total + 1 win + 0 天梯分分数加上10 - (232 - 与对手天梯分之差) 的20% 如果你的天梯分高 再减去对手低于你的分数差值的22%
        int winScore = 32 + (int) (Math.abs(loser.getScore() - winner.getScore()) * 0.2);
        if (loser.getScore() > winner.getScore()) winScore += (int) ((winScore - 30) * 0.6);
        //修改胜利者分数
        double winRate = (1.0 * (winner.getWinCount() + 1) / (winner.getTotalCount() + 1)) * 100;
        log.info(winId + " " + winScore + " " + winRate + " ");
        userMapper.updateUserWin(winId, winScore);
        int loserScore = 10 - (int) ((232 - (Math.abs(loser.getScore() - winner.getScore()))) * 0.2);
        if (loser.getScore() > winner.getScore()) loserScore -= (int)(Math.abs(loser.getScore() - winner.getScore())) * 0.22;
        //修改失败者分数
        winRate = (1.0 * loser.getWinCount() / (loser.getTotalCount() + 1)) * 100;
        log.info(loseId + " " + loserScore + " " + winRate + " ");
        userMapper.updateUserLose(loseId, loserScore);
        //释放房间对象
        log.info("已通知对手获胜!");
        roomManager.remove(room);
    }
}
