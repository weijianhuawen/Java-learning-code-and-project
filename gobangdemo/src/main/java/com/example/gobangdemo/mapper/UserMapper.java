package com.example.gobangdemo.mapper;

import com.example.gobangdemo.mode.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserMapper {
    // 注册
    void add(User user);
    // 登录
    User userSelectByName(String username);
    // 修改获胜者的信息 total + 1 win + 1 天体分数加上20 + 与对手天梯分之差的20% 如果对手天梯分高 差值加分幅度上升至30%
    void updateUserWin(@Param("uid") Integer uid, @Param("score") Integer score);
    // 修改失败者的信息 total + 1 win + 0 天梯分分数加上10 - (320 - 与对手天梯分之差) 的18% 如果你的天梯分高 再减去对手低于你的分数差值的12%
    void updateUserLose(@Param("uid")Integer uid, @Param("score")Integer score);
}
