package com.example.gobangdemo.mapper;

import com.example.gobangdemo.mode.User;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserMapperTest {

    @Autowired
    private UserMapper userMapper;
    @Test
    void updateUserWin() {
        int score = 10;
        double rate = 14.222222;
        userMapper.updateUserWin(2, score);
    }

    @Test
    void updateUserLose() {
        int score = -10;
        double rate = 14.222222;
        userMapper.updateUserWin(2, score);
    }
}