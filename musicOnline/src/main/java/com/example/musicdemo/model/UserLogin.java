package com.example.musicdemo.model;


import lombok.Data;

@Data
public class UserLogin {
    private Integer uid;
    private String username;
    private String password;
}
