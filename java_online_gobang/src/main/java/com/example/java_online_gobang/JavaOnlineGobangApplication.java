package com.example.java_online_gobang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaOnlineGobangApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaOnlineGobangApplication.class, args);
    }

}
