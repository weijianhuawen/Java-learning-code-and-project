//留言信息类
public class Message {
    //主动
    public String from;
    //被动
    public String to;
    //内容
    public String message;
    //随机词
    public String romAdv;
}
