package ui;

public class Menu {
    public void menu1() {
        System.out.println("*******************************");
        System.out.println("*****     学生课程管理系统    *****");
        System.out.println("*****       1学生管理       *****");
        System.out.println("*****       2课程管理       *****");
        System.out.println("*****      3课程成绩录入     *****");
        System.out.println("*****      4课程成绩情况     *****");
        System.out.println("*****      5学生成绩查询     *****");
        System.out.println("*****        -1退出        *****");
        System.out.println("*******************************");

    }

    public void menu2() {
        System.out.println("*******************************");
        System.out.println("*****        学生管理       *****");
        System.out.println("*****       1添加学生       *****");
        System.out.println("*****       2删除学生       *****");
        System.out.println("*****      3修改学生信息     *****");
        System.out.println("*****      4显示学生信息     *****");
        System.out.println("*****        -1退出        *****");
        System.out.println("*******************************");

    }

    public void menu3() {
        System.out.println("*******************************");
        System.out.println("*****      修改学生信息      *****");
        System.out.println("*****       1修改姓名       *****");
        System.out.println("*****       2修改性别       *****");
        System.out.println("*****       3修改年级       *****");
        System.out.println("*****       4修改专业       *****");
        System.out.println("*****       5修改班级       *****");
        System.out.println("*****        -1退出        *****");
        System.out.println("*******************************");

    }

    public void menu4() {
        System.out.println("*******************************");
        System.out.println("*****        课程管理       *****");
        System.out.println("*****       1添加课程       *****");
        System.out.println("*****       2删除课程       *****");
        System.out.println("*****      3修改课程信息     *****");
        System.out.println("*****      4显示课程信息     *****");
        System.out.println("*****        -1退出        *****");
        System.out.println("*******************************");

    }

    public void menu5() {
        System.out.println("*******************************");
        System.out.println("*****      修改课程信息      *****");
        System.out.println("*****       1修改名称       *****");
        System.out.println("*****       2修改性质       *****");
        System.out.println("*****       3修改类别       *****");
        System.out.println("*****       4修改学分       *****");
        System.out.println("*****       5修改学期       *****");
        System.out.println("*****        -1退出        *****");
        System.out.println("*******************************");
    }
}
