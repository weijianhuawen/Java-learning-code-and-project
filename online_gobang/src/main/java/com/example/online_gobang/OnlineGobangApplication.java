package com.example.online_gobang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineGobangApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineGobangApplication.class, args);
	}

}
