import java.util.TreeMap;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) {
        Integer integer = Integer.parseInt("01");

        System.out.println(integer);
        TreeMap<String, Integer> treeMap = new TreeMap<>((a, b) -> {
           return 0;
        });
        TreeSet treeSet = new TreeSet();

    }
}
class Food {
    public String name;
    public String type;
    public Integer rate;

    public Food() {};

    public Food(String name, String type, Integer rate) {
        this.name = name;
        this.type = type;
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "Food{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", rate=" + rate +
                '}';
    }
}