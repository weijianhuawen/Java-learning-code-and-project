import java.util.Arrays;

public class TestDemo {
    public static int onlyOne(int[] arr) {
        if (arr == null) {
            return 0;
        }
        int ret = 0;
        for (int i = 0; i < arr.length; i++) {
            ret ^= arr[i];
        }
        return ret;
    }

    public static void main(String[] args) {
        //给定一个非空整数数组，除了某个元素只出现一次以外，其余每个元素均出现两次。找出那个只出现了一次的元素。
        int[] arr1 = {2,2,1};
        int[] arr2 = {4,1,2,1,2};
        System.out.println(onlyOne(arr1));
        System.out.println(onlyOne(arr2));

    }
    public static String myToString(int[] arr) {
        if (arr == null) {
            return "[]";
        }
        String str = "[";
        for (int i = 0; i < arr.length - 1; i++) {
            str += arr[i] + ",";
        }
        str += arr[arr.length - 1] + "]";
        return str;
    }

    public static void main10(String[] args) {
        //实现一个方法 toString, 把一个整型数组转换成字符串. 例如数组 {1, 2, 3} , 返回的字符串为 "[1, 2, 3]", 注意 逗号 的位置和数量.
        int[] arr = {1,2,3,4,5,6,7,8,9};
        System.out.println(myToString(arr));
    }
    public static int[] copyOf(int[] arr) {
        if (arr == null) {
            return null;
        }
        int[] narr = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            narr[i] = arr[i];
        }
        return narr;
    }
    public static void main9(String[] args) {
        //实现一个方法 copyOf, 对一个整型数组进行拷贝, 得到一个新的数组.
        int[] arr1 = {1,2,3,4,5,6,7,8,9,10,11,12};
        System.out.println(Arrays.toString(arr1));
        System.out.println("拷贝数组：");
        int[] arr2 = copyOf(arr1);
        System.out.println(Arrays.toString(arr2));
    }
    public static int binary(int[] arr, int k) {
        if (arr == null) {
            return -1;
        }
        int left = 0;
        int right = arr.length - 1;
        int mid  = left + (right - left) / 2;
        while (left <= right) {
            mid = left + (right - left) / 2;
            if (arr[mid] > k) {
                right = mid - 1;
            }
            else if (arr[mid] < k) {
                left = mid + 1;
            }
            else {
                return mid;
            }
        }
        return -1;
    }
    public static void isSearch(int index) {
        if (index >= 0) {
            System.out.println("找到了，下标为：" + index);
        }
        else {
            System.out.println("没找到！");
        }
    }
    public static void main8(String[] args) {
        //给定一个有序整型数组, 实现二分查找
        int[] arr = {2,4,5,6,8,9,11,13,14,18,22,33};
        int i = binary(arr,13);//找得到
        int j = binary(arr, 23);//找不到
        isSearch(i);
        isSearch(j);
    }
    public static boolean isSort(int[] arr) {
        if (arr == null) {
            return false;
        }
        int cnt1 = 1;
        int cnt2 = 1;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] < arr[i+1]) {
                cnt1++;
            }
            if (arr[i] > arr[i+1]) {
                cnt2++;
            }
        }
        if (cnt1 == arr.length || cnt2 == arr.length) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 如果只需要判断递增数组
     * @param arr
     */
    public static boolean isSortIn(int[] arr) {
        if (arr == null) {
            return false;
        }
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] > arr[i+1]) {
                return false;
            }
        }
        return true;
    }
    public static void main7(String[] args) {
        //给定一个整型数组, 判定数组是否有序
        int[] arr1 = {1,2,3,4,5,6,7,8};
        int[] arr2 = {1,3,5,2,9,10,4,6};
        int[] arr3 = {8,7,6,5,4,3,2,1};
        System.out.println(isSortIn(arr1));//true
        System.out.println(isSortIn(arr2));//false
        System.out.println(isSortIn(arr3));//false
        System.out.println(isSort(arr1));//true
        System.out.println(isSort(arr2));//false
        System.out.println(isSort(arr3));//true
    }
    public static void bubbleSort(int[] data) {
        if (data == null)
            return;
        for (int i = 0; i < data.length - 1; i++) {
            boolean flag = true;
            for (int j = 0; j < data.length - 1 - i; j++) {
                if (data[j] > data[j+1]) {
                    int tmp = data[j];
                    data[j] = data[j+1];
                    data[j+1] = tmp;
                    flag = false;
                }
            }
            if (flag) {
                break;
            }
        }
    }
    public static void main6(String[] args) {
        //给定一个整型数组, 实现冒泡排序(升序排序)
        int[] arr = {45,23,54,12,34,26,98,13,3,22,4,33};
        System.out.println("排序前：");
        System.out.println(Arrays.toString(arr));
        bubbleSort(arr);
        System.out.println("排序后：");
        System.out.println(Arrays.toString(arr));
    }
    public static void main5(String[] args) {
        //创建一个 int 类型的数组, 元素个数为 100, 并把每个元素依次设置为 1 - 100
        int[] arr = new int[100];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = i+ 1;
        }
        System.out.println(Arrays.toString(arr));
    }
    public static void printArray(int[] arr) {
        if (arr == null) {
            System.out.println("传入的引用变量不能为空！");
            return;
        }
        for (int x:arr) {
            System.out.print(x + "  ");
        }
    }
    public static void main4(String[] args) {
        //实现一个方法 printArray, 以数组为参数, 循环访问数组中的每个元素, 打印每个元素的值.
        int[] arr = {1,2,3,4,5,6,7,8,9,10};
        printArray(arr);
    }
    public static void transform(int[] data) {
        if (data == null)
            return;
        for (int i = 0; i < data.length; i++) {
            data[i] *= 2;
        }
    }
    public static void main3(String[] args) {
        //实现一个方法 transform, 以数组为参数, 循环将数组中的每个元素 乘以 2 , 并设置到对应的数组元素上. 例如 原数组为 {1, 2, 3}, 修改之后为 {2, 4, 6}
       int[] arr = {1,2,3,4,5,6,7,8};
       transform(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "  ");
        }
    }
    public static double sum(double[] data) {
        if (data == null) {
            return  0;
        }
        double sum = 0;
        for (int i = 0; i < data.length; i++) {
            sum += data[i];
        }
        return sum;
    }
    public static void main2(String[] args) {
        //实现一个方法 sum, 以数组为参数, 求数组所有元素之和.
        double[] arr = {1,2,3,4,5,6,7};
        System.out.println(sum(arr));
    }
    public static double avg(int[] data) {
        if (data == null) {
            return 0;
        }
        int sum  = 0;
        for (int i = 0; i < data.length; i++) {
            sum += data[i];
        }
        return 1.0 * sum / data.length;
    }
    public static double avg(double[] data) {
        if (data == null) {
            return 0;
        }
        double sum  = 0;
        for (int i = 0; i < data.length; i++) {
            sum += data[i];
        }
        return sum / data.length;
    }
    public static void main1(String[] args) {
        //实现一个方法 avg, 以数组为参数, 求数组中所有元素的平均值(注意方法的返回值类型).
        int[] arr1 = {1,2,4,6,12};
        System.out.println(avg(arr1));
        double[] arr2 = {3.14,9.88,4.22,45.5,23.9};
        System.out.println(avg(arr2));
    }
}
