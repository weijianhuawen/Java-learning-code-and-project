import java.util.*;

public class Main {
    private static final int N = 100000 + 2333;
    //储存元素
    private static int[] e = new int[N];
    //储存next域的下标
    private static int[] en = new int[N];
    //储存prev域的下标
    private static int[] ep = new int[N];

    //0下标位置为头结点 1下标位置为尾巴结点
    private static int head = 0;
    private static int tail = 1;
    //当前可插入元素的下标
    private static int idx = 2;

    //初始化
    private static void init() {
        head = 0;
        tail = 1;
        en[head] = tail;
        ep[tail] = head;
        idx = 2;
    }

    //头插
    private static void headAdd(int x) {
        e[idx] = x;
        en[idx] = en[head];
        ep[idx] = head;
        ep[en[head]] = idx;
        en[head] = idx++;
    }

    //尾插
    private static void tailAdd(int x) {
        e[idx] = x;
        en[idx] = tail;
        ep[idx] = ep[tail];
        en[ep[tail]] = idx;
        ep[tail] = idx++;
    }

    //删除
    private static void del(int k) {
        //修改next域
        en[ep[k]] = en[k];
        //修改prev域
        ep[en[k]] = ep[k];
    }

    //在k后面插入
    private static void insert(int k, int x) {
        e[idx] = x;
        en[idx] = en[k];
        ep[idx] = k;
        ep[en[k]] = idx;
        en[k] = idx++;
    }
    //在k前面插入
    private static void insertFont(int k, int x) {
        insert(ep[k], x);
    }

    //输出链表元素
    private static void display() {
        int cur = en[head];
        int cnt = 100;


        while (cur != tail && cnt-- > 0) {
            System.out.print(cur +":");
            System.out.print(e[cur] + " ");

            cur = en[cur];
        }
    }

    //链表逆序输出
    private static void redisplay() {
        int cur = ep[tail];

        while (cur != head) {
            System.out.print(e[cur] + " ");
            cur = ep[cur];
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int m = sc.nextInt();
        sc.nextLine();

        //初始化
        init();

        while (m-- > 0) {
            String s = sc.nextLine();
            String[] ss = s.split(" ");
            int k, x;
            String op = ss[0];

            if ("L".equals(op)) {
                //头插
                x = Integer.parseInt(ss[1]);
                headAdd(x);
            } else if ("R".equals(op)) {
                //尾插
                x = Integer.parseInt(ss[1]);
                tailAdd(x);
            } else if ("D".equals(op)) {
                //删除k位置的结点
                k = Integer.parseInt(ss[1]);
                //System.out.print(k + " ");
                del(k + 1);
            } else if ("LR".equals(op)) {
                //插入第k个插入元素的前面 下标从2开始插入 第一个插入的数的k为1，因此需传入k+1
                k = Integer.parseInt(ss[1]);
                x = Integer.parseInt(ss[2]);
                insertFont(k + 1, x);
            } else if ("DR".equals(op)) {
                //插入到第k个插入元素的后面
                k = Integer.parseInt(ss[1]);
                x = Integer.parseInt(ss[2]);
                insert(k + 1, x);
            }
        }
        //输出链表结果
        display();
    }
}