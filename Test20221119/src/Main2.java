import java.util.Scanner;

public class Main2 {
    /**
     * 某系统的数字密码大于0，比如1983，采用加密方式进行传输
     * 规则如下：先得到每位数，然后每位数都加上5，再对10求余，最后将所有数字反转，得到一串新数
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        //得到每一位数
        int ret = 0;
        while (n > 0) {
            //获取个位
            int t = n % 10; // 获取个位
            t += 5; // 获取的位加5
            t %= 10; // 8
            ret = ret * 10 + t; // 8
            //更新n
            n /= 10;
        }
        System.out.println(ret); // 6438
    }
}
