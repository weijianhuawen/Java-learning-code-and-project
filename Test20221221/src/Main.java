import java.util.*;

public class Main {
    public static void main(String[] args) {
        String[] s = {"零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖", "拾"};
        String[] d = {"元", "拾", "佰", "仟", "万", "拾", "佰"};
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        StringBuilder ans = new StringBuilder();
        int len = 7;
       if (n < 0) {
           System.out.println("数据不合法!");
           return;
       }
       while (len-- > 0) {
           int t = 0;
           String ss = s[t];
           if (n > 0)  {
               t = n % 10;
               ss = s[t];
               n /= 10;
           }
           ss += d[6 - len];
           ans.insert(0, ss);
       }

        System.out.println(ans);
    }
}
