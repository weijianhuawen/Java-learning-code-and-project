import java.util.Scanner;
import java.util.*;
// 1:无需package
// 2: 类名必须Main, 不可修改

public class Main {
    private static int[] mToD = new int[]{0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    //判断闰年
    private static boolean isOk(int y) {
        return (y % 4 == 0 && y % 100 != 0) || (y % 400 == 0);
    }
    private static int[] transDate(String s) {
        int[] res = new int[3];
        char[] cs = s.toCharArray();
        res[0] = (cs[0] - '0') * 1000 + (cs[1] - '0') * 100 + (cs[2]- '0') * 10 + (cs[3] - '0');
        res[1] = (cs[4] - '0') * 10 + (cs[5] - '0');
        res[2] = (cs[6] - '0') * 10 + (cs[7] - '0');
        return res;
    }

    //判断回文
    private static boolean isPd(String s) {
        char[] cs = s.toCharArray();
        int l = 0;
        int r = cs.length - 1;
        while (l < r) {
            if (cs[l] != cs[r]) return false;
            l++;
            r--;
        }
        return true;
    }

    private static String transString(int[] date) {
        StringBuilder sb = new StringBuilder();
        String ts = "0000" + date[0];
        sb.append(ts.substring(ts.length() - 4, ts.length()));
        ts = "0" + date[1];
        sb.append(ts.substring(ts.length() - 2, ts.length()));
        ts = "0" + date[2];
        sb.append(ts.substring(ts.length() - 2, ts.length()));

        return sb.toString();
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //在此输入您的代码...
        String s = scan.nextLine();

        int[] date = transDate(s);
        //模拟
        int cnt = 0;
        while (cnt < 2) {
            //System.out.println(Arrays.toString(date));
            if (isPd(transString(date))) {
                System.out.println(transString(date));
                cnt++;
            }
            if (isOk(date[0])) mToD[2] = 29;
            else mToD[2] = 28;

            if (date[2] >= mToD[date[1]]) {
                date[2] = 1;
                date[1]++;
                if (date[1] == 13) {
                    date[1] = 1;
                    date[0]++;
                }
            } else {
                date[2]++;
            }
        }

        scan.close();
    }
}