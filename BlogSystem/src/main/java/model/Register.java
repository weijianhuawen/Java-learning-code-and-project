package model;

public class Register {
    //注册用户名
    private String username;
    //注册密码
    private String password;
    //注册确认密码
    private String ispassword;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getIspassword() {
        return ispassword;
    }
}
