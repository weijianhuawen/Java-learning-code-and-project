import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        char[] cs = new char[52];
        for (int i = 0; i < 26; i++) {
            cs[i] = (char) ('a' + i);
        }
        for (int i = 26; i < 52; i++) {
            cs[i] = (char) ('A' + i - 26);
        }
        System.out.println(Arrays.toString(cs));

        Random random = new Random();

        int idx = random.nextInt(52);

        System.out.println(cs[idx]);
    }
}
