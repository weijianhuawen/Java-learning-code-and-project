public class CompleteKnapsack {
    //动态规划实现完全背包代码

    /**
     *
     * @param n 物品个数
     * @param c 背包的总容量
     * @param w 每种物品的价值
     * @param v 每种物品的容量
     * @return 能将物品放进背包的最大价值
     */
    public int cknapsack1(int n, int c, int[] w, int[] v) {
        //状态定义f[i][j]表示最大价值
        int[][] f = new int[n][c + 1];
        //确定初始状态
        for (int j = 1; j <= c; j++) {
            int k = j / v[0];
            f[0][j] = k * w[0];
        }
        //状态转移f[i][j]=max(f[i-1][j - k * v[i]]+k*w[i]
        for (int i = 1; i < n; i++) {
            int val = w[i];
            for (int j = 1; j <= c; j++) {
                int cur = 0;
                for (int k = 0; j >= k * v[i]; k++) {
                    int t = f[i - 1][j - k * v[i]] + k * val;
                    cur = Math.max(t, cur);
                }
                f[i][j] = cur;
            }
        }
        return f[n - 1][c];
    }

    /**
     *
     * @param n 物品个数
     * @param c 背包的总容量
     * @param w 每种物品的价值
     * @param v 每种物品的容量
     * @return 能将物品放进背包的最大价值
     */
    public int cknapsack2(int n, int c, int[] w, int[] v) {
        //状态定义f[i][j]表示最大价值 滚动数组优化
        int[][] f = new int[2][c + 1];
        //确定初始状态
        for (int j = 1; j <= c; j++) {
            int k = j / v[0];
            f[0][j] = k * w[0];
        }
        //状态转移f[i][j]=max(f[i-1][j - k * v[i]]+k*w[i]
        for (int i = 1; i < n; i++) {
            int val = w[i];
            int ci = i & 1;
            int pi = (i - 1) & 1;
            for (int j = 1; j <= c; j++) {
                int cur = 0;
                for (int k = 0; j >= k * v[i]; k++) {
                    int t = f[pi][j - k * v[i]] + k * val;
                    cur = Math.max(t, cur);
                }
                f[ci][j] = cur;
            }
        }
        return f[(n - 1) & 1][c];
    }


    /**
     *
     * @param n 物品个数
     * @param c 背包的总容量
     * @param w 每种物品的价值
     * @param v 每种物品的容量
     * @return 能将物品放进背包的最大价值
     */
    public int cknapsack3(int n, int c, int[] w, int[] v) {
        //状态定义f[i][j]表示最大价值 一维数组优化
        int[] f = new int[c + 1];
        //确定初始状态f[0]=0
        //状态转移
        for (int i = 0; i < n; i++) {
            for (int j = 0; j <= c; j++) {
                //不选择物品
                int nopt = f[j];
                //选择物品
                int opt = j >= v[i] ? f[j - v[i]] + w[i] : nopt;

                f[j] = Math.max(opt, nopt);
            }
        }
        return f[c];
    }
}
