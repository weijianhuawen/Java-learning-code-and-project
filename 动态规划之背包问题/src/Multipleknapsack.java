import java.util.ArrayList;
import java.util.List;

public class Multipleknapsack {
    /**
     *
     * @param n 物品数
     * @param c 容量
     * @param v 每件物品的体积
     * @param w 每件物品的价值
     * @param s 每件物品的数量
     * @return 最大价值
     */
    public int multipleKS(int n, int c, int[] v, int[] w, int[] s) {
        //状态定义 定义f[i][j]表示选择前i件物品 在不超过背包容量j的情况下的最大价值
        int[][] f = new int[n][c + 1];

        //确地初始状态
        for (int j = 1; j <= c; j++) {
            int maxK = j / v[0];
            int k = Math.min(maxK, s[0]);
            f[0][j] = k * w[0];
        }
        //状态转移 f[i][j]=max(f[i-1][j-k*v[i]])
        for (int i = 1; i < n; i++) {
            int t = v[i];
            for (int j = 0; j <= c; j++) {
                //不选
                f[i][j] = f[i - 1][j];
                //选择k件
                for (int k = 1; k * t <= j && k <= s[i]; k++) {
                    f[i][j] = Math.max(f[i][j], f[i - 1][j - k * t] + k * w[i]);
                }
            }
        }
        return f[n - 1][c];
    }

    /**
     *
     * @param n 物品数
     * @param c 容量
     * @param v 每件物品的体积
     * @param w 每件物品的价值
     * @param s 每件物品的数量
     * @return 最大价值
     */
    public int multipleKS2(int n, int c, int[] v, int[] w, int[] s) {
        //状态定义 定义f[i][j]表示选择前i件物品 在不超过背包容量j的情况下的最大价值 滚动数组优化
        int[][] f = new int[2][c + 1];

        //确地初始状态
        for (int j = 1; j <= c; j++) {
            int maxK = j / v[0];
            int k = Math.min(maxK, s[0]);
            f[0][j] = k * w[0];
        }
        //状态转移 f[i][j]=max(f[i-1][j-k*v[i]])
        for (int i = 1; i < n; i++) {
            int t = v[i];
            int pre = (i - 1) & 1;
            int cur = i & 1;
            for (int j = 0; j <= c; j++) {
                //不选
                f[cur][j] = f[pre][j];
                //选择k件
                for (int k = 1; k * t <= j && k <= s[i]; k++) {
                    f[cur][j] = Math.max(f[cur][j], f[pre][j - k * t] + k * w[i]);
                }
            }
        }
        return f[(n - 1) & 1][c];
    }

    /**
     *
     * @param n 物品数
     * @param c 容量
     * @param v 每件物品的体积
     * @param w 每件物品的价值
     * @param s 每件物品的数量
     * @return 最大价值
     */
    public int multipleKS3(int n, int c, int[] v, int[] w, int[] s) {
        //状态定义 定义f[i][j]表示选择前i件物品 在不超过背包容量j的情况下的最大价值 一维数组优化
        int[] f = new int[c + 1];

        //确地初始状态
        for (int j = 1; j <= c; j++) {
            int maxK = j / v[0];
            int k = Math.min(maxK, s[0]);
            f[j] = k * w[0];
        }
        //状态转移 f[j]=max(f[j-k*t]+k*w[i] k~[0,s[i]]
        for (int i = 1; i < n; i++) {
            int t = v[i];
            for (int j = c; j >= t; j--) {
                //选择k件
                for (int k = 1; k * t <= j && k <= s[i]; k++) {
                    f[j] = Math.max(f[j], f[j - k * t] + k * w[i]);
                }
            }
        }
        return f[c];
    }

    /**
     *
     * @param n 物品数
     * @param c 容量
     * @param v 每件物品的体积
     * @param w 每件物品的价值
     * @param s 每件物品的数量
     * @return 最大价值
     */
    public int multipleKS4(int n, int c, int[] v, int[] w, int[] s) {
        //状态定义 定义f[i][j]表示选择前i件物品 在不超过背包容量j的情况下的最大价值 扁平化转换为0-1背包
        //使用list储存<v,w>，有多少个物品就存储多少个

        List<int[]> list = new ArrayList<>();
        //扁平化
        for (int i = 0; i < n; i++) {
            int cnt = s[i];
            while (cnt-- > 0) {
                list.add(new int[]{v[i], w[i]});
            }
        }
        int[] f = new int[c + 1];

        //确定初始状态 0-1模型 如果j>=vi0 则f[j]=wi0 其实可以不用初始化 在后面状态转移可以包含了初始状态过程
        //状态转移
        for (int i = 0; i < list.size(); i++) {
            int vi = list.get(i)[0];
            int wi = list.get(i)[1];
            for (int j = c; j >= vi; j--) {
                //选择与不选择选择价值大的
                f[j] = Math.max(f[j], f[j - vi] + wi);
            }
        }
        return f[c];
    }

}
