public class Main {
    public static void main(String[] args) {
        MyArrayList arrayList = new MyArrayList();
        arrayList.add(1);
        arrayList.add(3);
        arrayList.add(5);
        arrayList.add(7);
        arrayList.add(9);
        arrayList.print();
        arrayList.set(1, 12);
        arrayList.print();
        System.out.println(arrayList.search(7));
        System.out.println(arrayList.search(3));
        System.out.println("==============");
        arrayList.deleteByIndex(0);
        arrayList.print();
        arrayList.deleteByIndex(0);
        arrayList.print();
        arrayList.deleteByIndex(0);
        arrayList.print();
        arrayList.deleteByIndex(0);
        arrayList.print();
        arrayList.deleteByIndex(0);
        arrayList.print();


    }
}
