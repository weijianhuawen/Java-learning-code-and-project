public class MyArrayList {
    //数组
    private int[] elem;
    //有效元素个数
    private int size;
    //容量
    private int capatity;
    //默认容量
    private final int DEFAULT_CAPATITY = 10;

    public MyArrayList() {
        this.elem = new int[DEFAULT_CAPATITY];
        this.capatity = 10;
    }

    public void add(int val) {
        if (isFull()) {
            System.out.println("数组已满，插入失败!");
            return;
        }
        this.elem[this.size] = val;
        this.size++;
    }
    public boolean set(int index, int val) {
        //1.判断下标是否合法，不合法返回false
        if(index>=size||index<0){
            return false;
        }
        //2.如果合法就就修改
        elem[index]=val;
        return true;
    }
    public int delete() {
        //删除数组最后一个元素

        //1.判断数组是否为空，如果为空，return -1
        if(size==0){
            return -1;
        }

        //2.如果数组不为空，删除最后一个元素，并返回删除的元素是什么
        size--;
        return elem[size];
    }
    public int deleteByIndex(int index) {
        //1.判断index是否合法 -1
        if(index<0||index>=size){
            return -1;
        }
        //2.判断数组是否为空 -1
        if(size==0){
            return -1;
        }
        //3.删除index下标的元素，并返回
        if (size == 1) {
            int res = this.elem[size-1];
            size--;
            return res;
        }
        int em=elem[index];
        for(int i=index;i<=size-2;i++){

            elem[i]=elem[i+1];

        }
        size--;
        return em;
    }
    public boolean search(int val) {
        for(int i=0;i<=size-1;i++){
            if(elem[i]==val){
                return true;
            }
        }
        return false;
    }
    private boolean isFull() {
        return size == capatity;
    }
    public void print() {
        for (int i = 0 ; i < size; i++) {
            System.out.print(this.elem[i] + " ");
        }
        System.out.println();
    }

}
