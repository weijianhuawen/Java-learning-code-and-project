import java.util.Scanner;
import java.math.*;

public class Main {
    /**
     * @param n 需要判断的数
     * @return 是否为素数
     */

    public static boolean isPrime(int n) {
        if (n <= 1) return false;

        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) return false;
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        while (sc.hasNext()) {
            int n = sc.nextInt();
            System.out.println(isPrime(n));
        }
    }
}
